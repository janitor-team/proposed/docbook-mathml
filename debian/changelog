docbook-mathml (1.1CR1-3) unstable; urgency=medium

  * d/compat: Migrate to compat 11
  * d/rules: Remove explicit --parallel flag from dh sequence
  * d/control: Update Vcs URLs to new git location
  * d/control: Bump Std-Vers to 4.6.0 no changes needed
  * d/control: switch from w3-dtd-mathml to w3c-sgml-lib. Closes: #1001181
  * d/watch: Update to version 4

 -- Mathieu Malaterre <malat@debian.org>  Fri, 14 Jan 2022 15:45:29 +0100

docbook-mathml (1.1CR1-2.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Sun, 03 Jan 2021 15:46:03 +0100

docbook-mathml (1.1CR1-2) unstable; urgency=low

  * Rebuilt against sgml-base 1.26+nmu2. Closes: #675478

 -- Mathieu Malaterre <malat@debian.org>  Sat, 02 Jun 2012 18:19:50 +0200

docbook-mathml (1.1CR1-1) unstable; urgency=low

  * New candidate release
   - Add DTD DocBook MathML Module V1.1b1
   - Add DTD DocBook MathML Module V1.1CR1

 -- Mathieu Malaterre <malat@debian.org>  Wed, 23 May 2012 11:45:16 +0200

docbook-mathml (1.0.0-4) unstable; urgency=low

  * Properly install XML catalogs. Closes: #672433

 -- Mathieu Malaterre <malat@debian.org>  Fri, 11 May 2012 17:22:46 +0200

docbook-mathml (1.0.0-3) unstable; urgency=low

  * Add myself as Uploaders
  * Move Maintainer to: Debian XML/SGML Group
  * debian/control (Vcs-Browser): Fixed location.
    (Uploaders): Removed Mark Johnson, who is probably MIA (closes: #540558).
    Many thanks for your work!
  * Switch to dpkg-source 3.0 (quilt) format
  * Fix Homepage in d/control. Closes: #615252
  * d/watch: added
  * Fix typo in docbook-mathml.xmlcatalogs. Closes: #247100

 -- Mathieu Malaterre <malat@debian.org>  Thu, 10 May 2012 15:42:43 +0200

docbook-mathml (1.0.0-2) unstable; urgency=low

  * Fixed /usr/share/sgml compatibility symlink.

 -- Mark Johnson <mrj@debian.org>  Fri, 30 Jan 2004 22:09:47 -0500

docbook-mathml (1.0.0-1) unstable; urgency=low

  * Added xml catalog support.
  * Migrated installation to /usr/share/xml.
  * Cleaned up build - use more debhelper tools.

 -- Mark Johnson <mrj@debian.org>  Wed, 28 Jan 2004 20:05:26 -0500

docbook-mathml (1.0-2) unstable; urgency=low

  * Fixed mislocated "." in long description (I think). Closes: bug#116812

 -- Mark Johnson <mrj@debian.org>  Fri,  7 Nov 2003 22:27:22 -0500

docbook-mathml (1.0-1) unstable; urgency=low

  * Initial Release. Closes: bug#85390

 -- Mark Johnson <mrj@debian.org>  Thu, 21 Jun 2001 00:16:15 -0400
